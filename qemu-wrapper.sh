#!/usr/bin/env bash

set -e

# QEMU Wrapper Script

function help() {
    echo "$0 [OPTIONS]"
    echo " [-h: help]"
    echo " [-f: format HDD image]"
    echo " [-s: start virtual machine]" 
    echo " [-i QEMU_VM_ISO_IMAGE_PATH: ISO path]"
    echo " [-d QEMU_VM_HDD_IMAGE_PATH: HDD path]"
}

while getopts "hfsi:d:" OPT; do
    case $OPT in
        "h") help; exit 0 ;;
        "f") ACTION="vm_format" ;;
        "s") ACTION="vm_launch" ;;
        "i") QEMU_VM_ISO_IMAGE_PATH="${OPTARG}" ;;
        "d") QEMU_VM_HDD_IMAGE_PATH="${OPTARG}" ;;
        "*") help; exit 1 ;;
    esac
done

QEMU="/usr/bin/qemu-system-x86_64"
if [ ! -x "${QEMU}" ]; then
    echo "Unable to execute QEMU '${QEMU}' binary"
    exit 1
fi

if [ -z "${QEMU_VM_HDD_IMAGE_PATH}" ]; then
    if [ -f "qemu.img" ]; then
        QEMU_VM_HDD_IMAGE_PATH="qemu.img"
    else
        QEMU_VM_HDD_IMAGE_PATH="${XDG_DATA_HOME}/qemu.img"
    fi
fi

if [ -f "qemu-wrapper.env" ]; then
    QEMU_WRAPPER_ENV="qemu-wrapper.env"
else
    QEMU_WRAPPER_ENV="${XDG_CONFIG_HOME}/qemu-wrapper.env"
fi

. "$QEMU_WRAPPER_ENV" || ( echo "Unable to load environment"; exit 1 )

case $ACTION in
    "vm_format")
        echo "==> QEMU: Formatting HDD image ..."
        qemu-img create -f "${QEMU_VM_HDD_IMAGE_TYPE}" "${QEMU_VM_HDD_IMAGE_PATH}" ${QEMU_VM_HDD_IMAGE_SIZE}
    ;;
    "vm_launch")
        echo "==> QEMU: Starting virtual machine ..."
        if [ ! -z "$QEMU_VM_ISO_IMAGE_PATH" ]; then
            $QEMU -boot order=d -drive file=$QEMU_VM_HDD_IMAGE_PATH,format=$QEMU_VM_HDD_IMAGE_TYPE -m $QEMU_VM_RAM_SIZE $QEMU_VM_OPTIONS
        else
            $QEMU -boot order=d -drive file=$QEMU_VM_HDD_IMAGE_PATH,format=$QEMU_VM_HDD_IMAGE_TYPE -m $QEMU_VM_RAM_SIZE $QEMU_VM_OPTIONS -cdrom $QEMU_VM_ISO_IMAGE_PATH
        fi
    ;;
esac

exit 0
