## Quick VM

You can use this bash script to:
 - run your ISO or dumped HDD image
 - configure common options via .env file

# Usage

1. `git clone https://gitlab.com/jedi2light/Quick-VM.git`
2. `./install.sh`
3. `qemu-wrapper.sh -h`
