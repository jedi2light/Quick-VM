#!/usr/bin/env bash

set -ex

PREFIX="$HOME/.local"

install qemu-wrapper.sh ${PREFIX}/bin/ -m 0755
install qemu-wrapper.env ${HOME}/.config/ -m 0644

